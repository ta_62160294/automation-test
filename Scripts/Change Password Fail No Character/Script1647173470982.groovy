import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.applitools.eyes.metadata.ActualAppOutput
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable

import org.junit.Assert
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://myid.buu.ac.th/newchangepwd')

WebUI.setText(findTestObject('Change Password Fail No Character/input_(Username)_user'), '62160294')

WebUI.click(findTestObject('Change Password Fail No Character/button_Enter'))

WebUI.setEncryptedText(findTestObject('Change Password Fail No Character/input_(Old Password)_oldpass'), 'FCsYLKBhPHxTxZ5SdT2Zrw==')

WebUI.setEncryptedText(findTestObject('Change Password Fail No Character/input_(New Password)_newpass'), '26P8gjBb4b7fLdgIYStXzA==')

WebUI.setEncryptedText(findTestObject('Change Password Fail No Character/input_(Re-New Password)_renewpass'), '26P8gjBb4b7fLdgIYStXzA==')

WebUI.click(findTestObject('Change Password Fail No Character/button_Change Password'))

actualResult = WebUI.getText(findTestObject('Change Password Fail No Character/div_Not found characters in your new password'))
Assert.assertEquals('ไม่พบตัวอักษรภาษาอังกฤษ ในรหัสผ่านใหม่ของท่าน\nNot found characters in your new password', actualResult)

WebUI.closeBrowser()