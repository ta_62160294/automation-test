<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>f2a130c8-6f03-441c-9ba2-4cdb0cab0e3e</testSuiteGuid>
   <testCaseLink>
      <guid>be4d939c-0af5-4711-a450-6249721ad8d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>178b6768-20ef-47f2-a3ee-0f23f9781496</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aad94e23-5513-4a81-97ed-69dbaefb14a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Fail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4fd807f1-686f-4d97-bc51-5a39a826a36a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password Fail 8-25 character</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e308d4e-1f11-4d54-9473-0aefa6fdfc12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password Fail No Character</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0cb4f571-d375-4aed-a391-fb4ef9c97c3c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password Fail No Number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>410402be-ed7a-4123-a23a-811b0443d6f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password Fail No Symbol</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a3fb0ae4-c538-406a-8ca9-84d24e03ff55</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password Success</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
