<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>form_(Username)                            _f5a801</name>
   <tag></tag>
   <elementGuidId>f3b27422-ecd6-4c1f-97fb-48b9aa9813a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>form</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@action='https://myid.buu.ac.th/profile/signin']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>method</name>
      <type>Main</type>
      <value>post</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>https://myid.buu.ac.th/profile/signin</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                
                    บัญชีผู้ใช้ (Username)
                    
                
                
                    รหัสผ่าน (Password)
                    
					Show Password
                
                
                    เข้าสู่ระบบ / Sign in  
							ลืมรหัสผ่าน / reset password
						
                
                
                
                                  
            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12&quot;]/div[1]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/form[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@action='https://myid.buu.ac.th/profile/signin']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div/div/div/div/form</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='เข้าสู่ระบบ (Sign In)'])[1]/following::form[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//form[(text() = '
                
                
                    บัญชีผู้ใช้ (Username)
                    
                
                
                    รหัสผ่าน (Password)
                    
					Show Password
                
                
                    เข้าสู่ระบบ / Sign in  
							ลืมรหัสผ่าน / reset password
						
                
                
                
                                  
            ' or . = '
                
                
                    บัญชีผู้ใช้ (Username)
                    
                
                
                    รหัสผ่าน (Password)
                    
					Show Password
                
                
                    เข้าสู่ระบบ / Sign in  
							ลืมรหัสผ่าน / reset password
						
                
                
                
                                  
            ')]</value>
   </webElementXpaths>
</WebElementEntity>
